const getData = require('../models/getDataRtdb');

const getDataRtdb = async (path) => getData(path);

module.exports = {
  getDataRtdb
};