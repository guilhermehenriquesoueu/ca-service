const createDoc = require('../models/createDocFirestore');
const findDocById = require('../models/findDocByIdFirestore');
const updateOneDoc = require('../models/updateOneDocFirestore');
const deleteManyDoc = require('../models/deleteDocFirestore');
const getDoc = require('../models/getDocFirestore');
const getAllDocs = require('../models/getAllDocsFirestore');

const createDocFirestore = async (collection, title, body) => createDoc(collection, title, body);
const findDocByIdFirestore = async (collection, filter) => findDocById(collection, filter);
// eslint-disable-next-line max-len
const updateOneDocFirestore = async (collection, filter, field) => updateOneDoc(collection, filter, field);
const deleteDocFirestore = async (collection, filter) => deleteManyDoc(collection, filter);
const getDocFirestore = async (collection, doc) => getDoc(collection, doc);
const getAllDocsFirestore = async (collection) => getAllDocs(collection);

module.exports = {
  createDocFirestore,
  findDocByIdFirestore,
  updateOneDocFirestore,
  deleteDocFirestore,
  getDocFirestore,
  getAllDocsFirestore,
};
