// eslint-disable-next-line consistent-returna
module.exports = async (req, res, next) => {
  try {
  req.session.destroy()
  res.status(200).end();
  } catch (err) {
    res.status(401).end();
  }
};
