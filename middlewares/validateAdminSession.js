// eslint-disable-next-line consistent-returna
module.exports = async (req, res, next) => {
  try {
    if(req.session.user.role != 'sP=F8L@&vuT0a') return res.status(401).end();
    if(req.session.user.clientIp != req.clientIp) return res.status(401).end();
    const { firstName, secondName, email, whatsapp, telegram, nextPayment, pId  } = req.session.user;
    res.status(200).send({ firstName, secondName, email, whatsapp, telegram, nextPayment, pId }).end();
  } catch (err) {
    res.status(401).end();
  }
};
