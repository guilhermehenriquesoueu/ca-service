const { v4: uuidv4, validate } = require('uuid');

const createUuid = () => {
  const token = uuidv4();
  return token;
};

const validateUuid = (uuid) => validate(uuid);

module.exports = {
  createUuid,
  validateUuid,
};
