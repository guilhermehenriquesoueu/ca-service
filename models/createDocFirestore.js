const con = require('../conns/indexCloudFirestore');
const logger = require('../modules/logger');

const createDocFirestore = async (collection, title, body) => {
  try {
    await con.dbFirestore.collection(collection).doc(title).set(body);
    return { status: true, scope: 'models=>createDocFirestore.js', message: 'Doc was created!' };
  } catch (err) {
    logger.error({ err, scope: '[Function]createDocFirestore => [Model]createDocFirestore.js' });
    return { status: false, scope: 'models=>createDocFirestore.js', message: 'Doc was not created!' };
  }
}

module.exports = createDocFirestore;
// createDocFirestore('users', {name:'Deborah', last:'Vieira', email: 'eudebvieira@gmail.com'});