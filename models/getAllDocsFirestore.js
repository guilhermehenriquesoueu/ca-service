const {dbFirestore} = require('../conns/indexCloudFirestore');
const logger = require('../modules/logger');

const getAllDocsFirestore = async (collection) => {
  try {
    let listOnlyDocs = [];
    const listDocs = await dbFirestore.collection(`${collection}`).get();
    if(listDocs._docs().length <= 0) return { status: false, message: 'Doc was not update!' };
    listDocs.forEach(doc => {
      listOnlyDocs.push(doc.data());
    });
    return { status: true, message: 'Doc was update!', data: listOnlyDocs};
  } catch (err) {
    logger.error({ err, scope: '[Function]deleteManyDoc => [Fi]mongoose.js' });
  }
}

module.exports = getAllDocsFirestore;