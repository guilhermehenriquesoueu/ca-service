const con = require('../conns/indexCloudFirestore');
const logger = require('../modules/logger');

const findDocByIdFirestore = async (collection, filter) => {
  try {
    const doc = await con.dbFirestore.collection(`${collection}`).doc(`${filter}`).get();
    if(doc._fieldsProto == undefined) return { 
      status: false, 
      scope: 'models=>findDocByIdFirestore.js', 
      message: 'Doc was not find!'
    };
    return ({ 
      status: true, 
      scope: 'models=>findDocByIdFirestore.js', 
      message: 'Doc was find!',
      data: doc.data(),
    });
  } catch (err) {
    logger.error({ err, scope: '[Function]findDocByIdFirestore => [Model]findDocByIdFirestore.js' });
  }
}

module.exports = findDocByIdFirestore;