const {dbRealTimeDb} = require('../conns/indexCloudFirestore');
const logger = require('../modules/logger');

const getDataRtdb = async (path) => {
  try {
    return new Promise( (resolve, reject) => {
      const ref = dbRealTimeDb.ref(path);
      ref.once('value', (snapshot) => {
        resolve ({ status: true, scope: 'models=>getDataRtdb.js', data: snapshot.val()});
      });
    });
  } catch (err) {
    logger.error({ err, scope: '[Function]getDataRtdb => [Model]getDataRtdb.js' });
    return { status: false, scope: 'models=>getDataRtdb.js', message: 'Doc was not get!' };
  }
}

module.exports = getDataRtdb;