const con = require('../conns/indexCloudFirestore');
const logger = require('../modules/logger');

const deleteDocFirestore = async (collection, docTitle) => {
  try {
    await con.dbFirestore.collection(collection).doc(docTitle).delete();
    return { status: true, scope: 'models=>deleteDocFirestore.js', message: 'Doc was deleted!' };
  } catch (err) {
    logger.error({ err, scope: '[Function]deleteDocFirestore => [Model]deleteDocFirestore.js' });
    return { status: false, scope: 'models=>deleteDocFirestore.js', message: 'Doc was not deleted!' };
  }
}

module.exports = deleteDocFirestore;
// deleteDocFirestore('activationTokens', 'iamghgsbusiness@gmail.com');