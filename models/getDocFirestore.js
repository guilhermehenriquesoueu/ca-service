const {dbFirestore} = require('../conns/indexCloudFirestore');
const logger = require('../modules/logger');

const getDocFirestore = async (collection, document) => {
  try {
    const doc = dbFirestore.collection(collection).doc(document);
    const docData = await doc.get();
    if (!docData.exists) {
      return { status: false, message: 'Doc was not found!' };
    } else {
      return { status: true, message: 'Doc was not found!' , data: docData.data()};
    }
  } catch (err) {
    logger.error({ err, scope: '[Function]getDocFirestore => [Fi]getDocFirestore.js' });
  }
}

module.exports = getDocFirestore;
// getDocFirestore('serverConfig', 'rtdbKey')

