const con = require('../conns/indexCloudFirestore');
const logger = require('../modules/logger');

const updateOneDocFirestore = async (collection, filter, field) => {
  try {
    await con.dbFirestore.collection(`${collection}`).doc(`${filter}`).update(field);
    return { status: true, scope: 'models=>mongoose.js(updateDoc)', message: 'Doc was update!' };
  } catch (err) {
    logger.error({ err, scope: '[Function]updateOneDocFirestore => [File]updateOneDocFirestore.js' });
    return { status: false, scope: '[Function]updateOneDocFirestore => [File]updateOneDocFirestore.js', message: 'Doc was not update!' };
    }
}

module.exports = updateOneDocFirestore;
// updateOneDocFirestore('users', 'iamghgs@gmail.com', {email: 'group@kamartaj.com'});