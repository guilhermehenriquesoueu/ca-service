const nodemailer = require('nodemailer');
const logger = require('../logger');
const nunjucks = require('nunjucks');

const transporter = nodemailer.createTransport({
  host: 'email-smtp.us-east-2.amazonaws.com',
  port: 465,
  secure: true,
  auth: {
    user: 'AKIA35GHCSY4TUFQCN6P',
    pass: 'BAHZUZ5Y18JWRVezfCurMhh7sjhPQnk6SmlFcz1FcyPt',
  },
});


const emailActivation = async (toEmail, token) => {
  try {
    const templateActivation = nunjucks.render(__dirname + '/activation.njk', {email: toEmail, token});
    transporter.sendMail({
      from: 'Canto Estatistico <naoresponda@cantoestatistico.com>',
      to: `${toEmail}`, 
      subject: 'Ativação de conta', 
      html: templateActivation,
    });
  } catch (err) {
    logger.error({ err, scope: '[Fn]emailActivation => [Fi]emailSend.js' });
  }
};

const resetPassword = async (toEmail, token) => {
  try {
    const templatePassword = nunjucks.render(__dirname + '/password.njk', {email: toEmail, token});
    transporter.sendMail({
      from: 'Canto Estatistico <naoresponda@cantoestatistico.com>', // sender address
      to: `${toEmail}`,
      subject: 'Reset de senha', 
      html: templatePassword,
    });
   
  } catch (err) {
    logger.error({ err, scope: '[Fn]resetPassword => [Fi]emailSend.js' });
  }
};

module.exports = {
  emailActivation,
  resetPassword,
};
