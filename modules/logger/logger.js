const { Bristol } = require('bristol');
const palin = require('palin');
const moment = require('moment');

let logger;

const init = () => {
  try {
    logger = new Bristol();
    logger.addTarget('console').withFormatter(palin, {
      timestamp: (date) => moment(date).toISOString(),
      indent: '\n    ⇒  ',
    });
  return logger;
  } catch (err) {
    logger.error({ err, scope: '[Fn]init => [Fi]logger.js' });
  }
  
};

module.exports = {
  get instance() {
    return logger || init();
  },
};
