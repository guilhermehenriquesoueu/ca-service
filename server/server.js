const createServer = require('./createServer');
const logger = require('../modules/logger');

const { app } = createServer;
const portServer = process.env.PORT;

app.listen(portServer, () => {
  logger.info(`Running on ${portServer}`, { scope: 'NodeJS => server.js' });
});