if(process.env.ENV === 'dev'){
  const path = require('path');
  require('dotenv').config({ path: path.resolve(__dirname,'../secure/.env-dev') });
}

const {Firestore} = require('@google-cloud/firestore');
const express = require('express');
const session = require('express-session');
const app = express();
const {FirestoreStore} = require('@google-cloud/connect-firestore');
const server = require('http').createServer(app)
const cors = require('cors');
const requestIp = require('request-ip');
const helmet = require('helmet');


const createServer = () => {

  /* Get real client ip */
  app.use(requestIp.mw());
  /* End of configuration */

  /* Cors config */
  app.use(cors({
    origin: ['https://cantoestatistico.com', 'https://mrsakarov.cantoestatistico.com'],
    methods: ['POST', 'PUT', 'GET', 'PATCH', 'OPTIONS', 'HEAD'],
    credentials: true,
  }));
  /* End of configuration */

  /* Helmet */
  app.use(helmet());
 /* End of configuration */

  /* Session config */
  app.set('trust proxy', 1);
  app.use(session({
    name: 'sId',
    store: new FirestoreStore({
      dataset: new Firestore(),
      kind: 'sessions',
    }),
    secret: process.env.COOKIE_SECRET,
    resave: false,
    saveUninitialized: false,
    cookie: { 
      domain: '.cantoestatistico.com',
      secure: true,
      httpOnly: true,
      maxAge: 28800000 
    },
  }));
  /* End of configuration
  
  /* Middleware used to send information from frotend to backend */
  app.use(express.json());
  app.use(express.urlencoded({ extended: true }));
  /* End of configuration */

  /* End of configuration */

  /* Importing ROUTE FILE and passing APP as parameter */
  // eslint-disable-next-line global-require
  require('../src/routes')(app);
  /* End of configuration */

  return server;
};

module.exports = {
  app: createServer(),
};
