/* eslint-disable global-require */
module.exports = (app) => {
  const {create} = require('./services/create.user');
  const {activation} = require('./services/activation.user');
  const {login} = require('./services/login.user');
  const {loginAdmin} = require('./services/login.admin');
  const sessionAuth = require('../middlewares/validateSession');
  const sessionAdminAuth = require('../middlewares/validateAdminSession');
  const {resetPassword} = require('./services/reset.password.user');
  const {changePassword} = require('./services/change.password.user');
  const {update} = require('./services/update.user');
  const {setPayment} = require('./services/set.payment');
  const {getUsers} = require('./services/get.users');
  const {setConfig} = require('./services/set.config');
  const {getConfig} = require('./services/get.config');
  const {getAllConfigs} = require('./services/get.all.configs');
  const {updateConfig} = require('./services/update.config');
  const destroySession = require('../middlewares/destroySession');
  const {getMarkets} = require('./services/get.markets');
  

  app.post('/api/user', create);
  app.get('/api/user/:email/:token', activation);
  app.post('/api/login', login);
  app.post('/api/loginAdmin', loginAdmin);
  app.get('/api/validate', sessionAuth);
  app.get('/api/validateAdmin', sessionAdminAuth);
  app.get('/api/destroySession', destroySession);
  app.post('/api/password', resetPassword);
  app.post('/api/password/:email/:token', changePassword);
  app.put('/api/user', update);
  app.put('/api/payment', setPayment);
  app.post('/api/users', getUsers);
  app.put('/api/config', setConfig);
  app.post('/api/config', getConfig);
  app.post('/api/getallconfigs', getAllConfigs);
  app.patch('/api/config', updateConfig);
  app.post('/api/getmarkets', getMarkets)
};
