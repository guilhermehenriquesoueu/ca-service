const asyncHandler = require('../../helpers/asyncHandler');
const libFirestore = require('../../libs/libFirestore');
const logger = require('../../modules/logger');

// eslint-disable-next-line consistent-return
const update = asyncHandler(async (req, res) => {
  try {
    const { email, ...rest } = req.body;
    const paramFieldUpdateDoc = rest;
    const wasUpdated = await libFirestore.updateOneDocFirestore('users', email, paramFieldUpdateDoc);
    if (wasUpdated.status === false) return res.status(200).send({ status: false, message: 'Informações não poderam ser atualizadas!' }).end();
    return res.status(200).send({ status: true, message: 'Informações atualizadas!' }).end();
  } catch (error) {
    logger.error({ error, scope: '[Fn]update => [Fi]user.js' });
  }
});

module.exports = {
  update,
};
