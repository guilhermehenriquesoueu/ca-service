const asyncHandler = require('../../helpers/asyncHandler');
const libFirestore = require('../../libs/libFirestore');
const logger = require('../../modules/logger');

// eslint-disable-next-line consistent-return
const getAllConfigs = asyncHandler( async (req, res) => {
  try {
    if(req.session.user.role != 'sP=F8L@&vuT0a') return res.status(401).end();
    if(req.session.user.clientIp != req.clientIp) return res.status(401).end();
    const docs = await libFirestore.getAllDocsFirestore(req.body.collection);
    return res.status(200).send({ data: docs.data }).end();
  } catch (error) {
    logger.error({ error, scope: '[Fn]getAllConfigs => [Fi]getAllConfigs.js' });
  }
});

module.exports = {
  getAllConfigs,
};
