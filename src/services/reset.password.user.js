const asyncHandler = require('../../helpers/asyncHandler');
const libFirestore = require('../../libs/libFirestore');
const emailSend = require('../../modules/nodemailer/emailSend');
const uuid = require('../../helpers/uuid');
const logger = require('../../modules/logger');

// eslint-disable-next-line consistent-return
const resetPassword = asyncHandler(async (req, res) => {
  try {
    const { email } = req.body;
    const userWasFound = await libFirestore.findDocByIdFirestore('users', email);
    if (userWasFound.status === false) return res.status(200).send({ status: false, message: 'Enviamos um link para reset de senha no email informado!' }).end();
    const resetPasswordToken = uuid.createUuid();
    await libFirestore.createDocFirestore('passwordTokens', email, { token: resetPasswordToken });
    emailSend.resetPassword(email, resetPasswordToken);
    return res.status(200).send({ status: true, message: 'Enviamos um link para reset de senha no email informado!' }).end();
  } catch (error) {
    logger.error({ error, scope: '[Fn]resetPassword => [Fi]user.js' });
  }
});

module.exports = {
  resetPassword,
};
