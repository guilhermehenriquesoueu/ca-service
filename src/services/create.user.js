const bcrypt = require('bcrypt');
const asyncHandler = require('../../helpers/asyncHandler');
const libFirestore = require('../../libs/libFirestore')
const { v5: uuidv5 } = require('uuid');
const uuid = require('../../helpers/uuid');
const emailSend = require('../../modules/nodemailer/emailSend');
const logger = require('../../modules/logger');
const moment = require('moment-timezone');

// eslint-disable-next-line consistent-return
const create = asyncHandler(async (req, res) => {
  try {
    const {
      firstName, secondName, email, password, whatsapp, telegram,
    } = req.body;
    const wasFound = await libFirestore.findDocByIdFirestore('users', email);
    if (wasFound.status === true) return res.status(200).send({ isUserCreated: false, message: 'Endereço de email já registrado!' }).end();
    const hashPassword = bcrypt.hashSync(password, 10);
    const idUuidPersonal = uuidv5(email, '789cbc0c-a180-5170-98d5-3658a264ff05');
    const userCreated = moment().tz('America/Sao_Paulo').format();
    await libFirestore.createDocFirestore('users', email, {
      firstName,
      secondName,
      email,
      password: hashPassword,
      whatsapp,
      telegram,
      termsAccepted: true,
      isPaid: 'not',
      isActive: false,
      nextPayment: '',
      pId: idUuidPersonal,
      userCreated
    });
    const tokenActivation = uuid.createUuid();
    await libFirestore.createDocFirestore('activationTokens', email, {tokenActivation});
    emailSend.emailActivation(email, tokenActivation);
    return res.status(200).send({ isUserCreated: true, message: 'Enviamos um email de ativação de conta para o email fornecido.' }).end();
  } catch (error) {
    logger.error({ error, scope: '[Fn]createDoc => [Fi]user.js' });
  }
});

module.exports = {
  create,
};
