const asyncHandler = require('../../helpers/asyncHandler');
const libFirestore = require('../../libs/libFirestore');
const logger = require('../../modules/logger');
// eslint-disable-next-line consistent-return
const updateConfig = asyncHandler(async (req, res) => {
  try {
    if(req.session.user.role != 'sP=F8L@&vuT0a') return res.status(401).end();
    if(req.session.user.clientIp != req.clientIp) return res.status(401).end();
    const { title, ...rest } = req.body;
    const paramFieldUpdateDoc = rest;
    const wasUpdated = await libFirestore.updateOneDocFirestore('configs', title, paramFieldUpdateDoc);
    if (wasUpdated.status === false) return res.status(200).send({ status: false, message: 'Informações não poderam ser atualizadas!' }).end();
    return res.status(200).send({ status: true, message: 'Informações atualizadas!' }).end();
  } catch (error) {
    logger.error({ error, scope: '[Fn]update => [Fi]user.js' });
  }
});

module.exports = {
  updateConfig,
};
