const asyncHandler = require('../../helpers/asyncHandler');
const libFirestore = require('../../libs/libFirestore');
const logger = require('../../modules/logger');

// eslint-disable-next-line consistent-return
const activation = asyncHandler(async (req, res) => {
  try {
    const { email, token } = req.params;
    const tokenWasFound = await libFirestore.findDocByIdFirestore('activationTokens', email);
    if (tokenWasFound.status === false || tokenWasFound.data.tokenActivation !== token) return res.status(200).send({ status: false, message: 'Link inválido!' }).end();
    const paramFieldUpdateDoc = { isActive: true };
    const userWasActivate = await libFirestore.updateOneDocFirestore('users', email, paramFieldUpdateDoc);
    if (userWasActivate.status === false) return res.status(200).send({ status: false, message: 'Não foi possivel ativar sua conta. Contate o suporte!' }).end();
    await libFirestore.deleteDocFirestore('activationTokens', email);
    return res.status(200).send({ status: true, message: 'Usuário ativado com sucesso!' }).end();
  } catch (error) {
    logger.error({ error, scope: '[Fn]activation => [Fi]user.js' });
  }
});

module.exports = {
  activation,
};
