const moment = require('moment-timezone');
const libFirestore = require('../../libs/libFirestore');
const asyncHandler = require('../../helpers/asyncHandler');
const logger = require('../../modules/logger');


// eslint-disable-next-line consistent-return
const setPayment = asyncHandler(async (req, res) => {
  try {
    if(req.session.user.role != 'sP=F8L@&vuT0a') return res.status(401).end();
    if(req.session.user.clientIp != req.clientIp) return res.status(401).end();
    let valueOption;
    const option = (value) => {
      switch (value) {
        case 'trial':
          valueOption = 1;
          break;
        case 'standard':
          valueOption = 30;
          break;
        default:
          break;
      }
    };
    option(req.body.isPaid);
    if (req.body.isPaid === 'trial') {
      const dateNow = moment().tz('America/Sao_Paulo').add(valueOption, 'd').format();
      const paramFieldUpdateDoc = {
        nextPayment: dateNow,
        isPaid: req.body.isPaid,
        isAlreadyTrial: req.body.isAlreadyTrial,
      };
      const wasUpdated = await libFirestore.updateOneDocFirestore('users', req.body.email, paramFieldUpdateDoc);
      if (wasUpdated.status === false) return res.status(200).send({ status: false, message: 'Não foi possível atualizar esta informação!' }).end();
      return res.status(200).send({ status: true, message: 'Atualizado com sucesso!' }).end();
    }

    if (req.body.isPaid === 'standard') {
      const dateNow = moment().tz('America/Sao_Paulo').add(valueOption, 'd').format();
      const paramFieldUpdateDoc = { nextPayment: dateNow, isPaid: req.body.isPaid };
      const wasUpdated = await libFirestore.updateOneDocFirestore('users', req.body.email, paramFieldUpdateDoc);
      if (wasUpdated.status === false) return res.status(200).send({ status: false, message: 'Não foi possível atualizar esta informação!' }).end();
      return res.status(200).send({ status: true, message: 'Atualizado com sucesso!' }).end();
    }

    const paramFieldUpdateDoc = { nextPayment: '', isPaid: req.body.isPaid };
    const wasUpdated = await libFirestore.updateOneDocFirestore('users', req.body.email, paramFieldUpdateDoc);
    if (wasUpdated.status === false) return res.status(200).send({ status: false, message: 'Não foi possível atualizar esta informação!' }).end();
    return res.status(200).send({ status: true, message: 'Atualizado com sucesso!' }).end();
  } catch (error) {
    logger.error({ error, scope: '[Fn]changeStatusPayment => [Fi]user.js' });
  }
});

module.exports = {
  setPayment,
};
