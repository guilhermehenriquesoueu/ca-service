const asyncHandler = require('../../helpers/asyncHandler');
const logger = require('../../modules/logger');
const axios = require('axios');

// eslint-disable-next-line consistent-return
const getMarkets = asyncHandler(async (req, res) => {
  try {
    const { idMatch } = req.body;
    const data = await axios.get(`https://api.b365api.com/v2/event/odds?token=${process.env.BET365API_TOKEN}&event_id=${idMatch}`);
    res.status(200).send({status:true, data: data.data.results.odds}).end();
  } catch (error) {
    logger.error({ error, scope: '[Fn]getMarkets => [Fi]get.markets.js' });
  }
});

module.exports = {
  getMarkets,
};
