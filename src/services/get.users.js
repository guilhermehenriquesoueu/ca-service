const asyncHandler = require('../../helpers/asyncHandler');
const libFirestore = require('../../libs/libFirestore');
const logger = require('../../modules/logger');

// eslint-disable-next-line consistent-return
const getUsers = asyncHandler( async (req, res) => {
  try {
    if(req.session.user.role != 'sP=F8L@&vuT0a') return res.status(401).end();
    if(req.session.user.clientIp != req.clientIp) return res.status(401).end();
    const docs = await libFirestore.getAllDocsFirestore(req.body.collection);
    const users = [];
    // eslint-disable-next-line array-callback-return
    docs.data.map((i) => {
      const {
        id,
        firstName,
        secondName,
        email,
        whatsapp,
        telegram,
        terms,
        isPaid,
        isActive,
        nextPayment,
      } = i;
      users.push({
        id,
        firstName,
        secondName,
        email,
        whatsapp,
        telegram,
        terms,
        isPaid,
        isActive,
        nextPayment,
      });
    });
    return res.status(200).send({ data: users }).end();
  } catch (error) {
    logger.error({ error, scope: '[Fn]update => [Fi]user.js' });
  }
});

module.exports = {
  getUsers,
};
