const bcrypt = require('bcrypt');
const moment = require('moment');
const asyncHandler = require('../../helpers/asyncHandler');
const libFirestore = require('../../libs/libFirestore');
const libRtdb = require('../../libs/libRtdb');
const emailSend = require('../../modules/nodemailer/emailSend');
const uuid = require('../../helpers/uuid');
const logger = require('../../modules/logger');

// eslint-disable-next-line consistent-return
const loginAdmin = asyncHandler(async (req, res) => {
  try {
    const { email, password, mPassword } = req.body;
    const userFound = await libFirestore.findDocByIdFirestore('users', email );
    if (userFound.status === false) return res.status(200).send({ isAuthenticated: false, message: 'Email ou senha incorreta!' });
    const passwordIsValid = bcrypt.compareSync(password, userFound.data.password);
    if (!passwordIsValid) return res.status(200).send({ isAuthenticated: false, message: 'Email ou senha incorreta!' });
    const masterPassword = process.env.MASTER_ADMIN_PASSWORD;
    if (mPassword !== masterPassword) return res.status(200).send({ isAuthenticated: false, message: 'Email ou senha incorreta!' });
    if (userFound.data.role !== process.env.MASTER_ADMIN_ROLE) return res.status(200).send({ isAuthenticated: false, message: 'Email ou senha incorreta!' });
    if (userFound.data.isActive === false) {
      const tokenActivation = await libFirestore.findDocByIdFirestore('activationTokens', email);
      if (tokenActivation.status === false) {
        const newTokenActivation = uuid.createUuid();
        await libFirestore.createDocFirestore('activationTokens', email, {tokenActivation: newTokenActivation});
        emailSend.emailActivation(email, newTokenActivation);
        return res.status(200).send({ isAuthenticated: false, message: 'Usuário não verificado! Enviamos um novo email de ativação no endereço de email informado! ' });
      }
      emailSend.emailActivation(email, tokenActivation);
      return res.status(200).send({ isAuthenticated: false, message: 'Usuário não verificado! Enviamos um novo email de ativação no endereço de email informado! ' });
    }
    if (userFound.data.nextPayment === '') return res.status(200).send({ isAuthenticated: false, message: 'Não identificamos o pagamento de sua assinatura. Gentileza entrar em contato com o suporte através do Telegram @cantoestatistico!' });
    moment.locale('pt');
    const dateNow = moment();
    const nextPayment = moment(userFound.data.nextPayment);
    const diffMinutes = nextPayment.diff(dateNow, 'minutes');
    if (diffMinutes <= 0) {
      const paramFieldUpdateDoc = { nextPayment: '', isPaid: 'not' };
      await libFirestore.updateOneDocFirestore('users', userFound.data.email, paramFieldUpdateDoc);
      return res.status(200).send({ isAuthenticated: false, message: 'Sua assinatura expirou. Gentileza entrar em contato com o suporte através do do Telegram @cantoestatistico!' });
    }
    const {
      firstName, secondName, whatsapp, telegram, pId, role
    } = userFound.data;
    const userAlreadyLogged = await libRtdb.getDataRtdb(`/usersOnline/${pId}`);
    if (((userAlreadyLogged||{}).data||{}).state == 'online') return res.status(200).send({ isAuthenticated: false, message: 'O usuário já está logado!' });
    const clientIp = req.clientIp; 
    req.session.user = {
      firstName, secondName, email, whatsapp, telegram, clientIp, nextPayment, pId, role
    };
    res.status(200).send({ isAuthenticated: true}).end();
  } catch (error) {
    logger.error({ error, scope: '[Fn]login => [Fi]user.js' });
  }
});

module.exports = {
  loginAdmin,
};
