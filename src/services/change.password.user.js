const bcrypt = require('bcrypt');
const asyncHandler = require('../../helpers/asyncHandler');
const libFirestore = require('../../libs/libFirestore');
const logger = require('../../modules/logger');

// eslint-disable-next-line consistent-return
const changePassword = asyncHandler(async (req, res) => {
  try {
    const { password } = req.body;
    const { email, token } = req.params;
    const tokenWasFound = await libFirestore.findDocByIdFirestore('passwordTokens', email);
    if (tokenWasFound.data.token !== token) return res.status(200).send({ status: false, message: 'Link inválido!' }).end();
    const hashPassword = bcrypt.hashSync(password, 10);
    const paramFieldUpdateDoc = { password: hashPassword };
    await libFirestore.updateOneDocFirestore('users', email, paramFieldUpdateDoc);
    await libFirestore.deleteDocFirestore('passwordTokens', email);
    return res.status(200).send({ status: true, message: 'Senha alterada com sucesso!' }).end();
  } catch (error) {
    logger.error({ error, scope: '[Fn]changePassword => [Fi]user.js' });
  }
});

module.exports = {
  changePassword,
};
