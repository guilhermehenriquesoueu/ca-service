/* eslint-disable consistent-return */
const asyncHandler = require('../../helpers/asyncHandler');
const libFirestore = require('../../libs/libFirestore');
const logger = require('../../modules/logger');

const getConfig = asyncHandler(async (req, res) => {
  try {
    const { ref, sref } = req.body
    const doc = await libFirestore.getDocFirestore(ref, sref);
    if (doc.status === false) return res.status(200).send({ status: false, message: 'Não foi possivel encontrar as configurações!' });
    res.status(200).send({ status: true, data: doc.data, message: 'Configurações coletadas com sucesso!' }).end();
  } catch (error) {
    logger.error({ error, scope: '[Fn]getConfig => [Fi]config.js' });
  }
});

module.exports = {
  getConfig,
};
