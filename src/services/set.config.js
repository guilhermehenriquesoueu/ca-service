const asyncHandler = require('../../helpers/asyncHandler');
const libFirestore = require('../../libs/libFirestore');
const logger = require('../../modules/logger');

// eslint-disable-next-line consistent-return
const setConfig = asyncHandler(async (req, res) => {
  try {
    if(req.session.user.role != 'sP=F8L@&vuT0a') return res.status(401).end();
    if(req.session.user.clientIp != req.clientIp) return res.status(401).end();
    const { title } = req.body;
    const wasUpdated = await libFirestore.createDocFirestore('configs', title, req.body);
    if (wasUpdated.status === false) return res.status(200).send({ status: false, message: 'O campo não foi atualizado!' }).end();
    res.status(200).send({ status: true, message: 'O campo foi atualizado!' }).end();
  } catch (error) {
    logger.error({ error, scope: '[Fn]config => [Fi]config.js' });
  }
});

module.exports = {
  setConfig,
};
