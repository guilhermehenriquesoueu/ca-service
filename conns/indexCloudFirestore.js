const admin = require('firebase-admin');
const serviceAccount = require('../secure/firebase-firestore.json');
const logger = require('../modules/logger');

try {
  admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
  });

  const realTimeDb = admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://canto-estatistico-eua-331403-default-rtdb.firebaseio.com",
  }, 'RTDB');

  const dbFirestore = admin.firestore();
  const dbRealTimeDb = realTimeDb.database();
  
  module.exports = {
    dbFirestore,
    dbRealTimeDb,
  };
} catch (err) {
  logger.error({ err, scope: 'Cloud Firestore OFF => indexCloudFirestore.js' });
}